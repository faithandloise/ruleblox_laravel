<!DOCTYPE html>
<html>

<head>

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <link rel='shortcut icon' type='image/x-icon' href='favicon.ico' />


    @extends('header')

</head>

<body>

<!-- Navigation -->
<header id="top" class="header">
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><img src="" alt="Ruleblox" /></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right custom-menu">
                    <li class="active"><a href="#home">Home</a></li>
                    <li><a href="#about">About</a></li>
                    <li><a href="#services">Dashboard</a></li>
                    <li><a href="#contact">Contact</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>

<!-- Full Page Image Background Carousel Header -->
<header id="mainCarousel" class="carousel slide">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#mainCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#mainCarousel" data-slide-to="1"></li>
        <li data-target="#mainCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for Slides -->
    <div class="carousel-inner">
        <div class="item active">
            <div class="slider_overlay"></div>
            <!-- Set the first background image using inline CSS below. -->
            <div class="fill" style="background-image:url('../img/slide1.jpg');">
                <div class="hero">
                    <hgroup>
                        <h1>RuleBlox</h1>
                        <h3> Sensors as a Service</h3>

                    <div class="text-vertical-center form-group col-sm-6 col-sm-offset-4">
                        <a href="#about" class="btn btn-dark btn-lg"><b>Learn More</b></a>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-6 col-sm-offset-4">
                            {!! HTML::link(url('/auth/login'), Lang::get('Login')) !!}
                        </div>
                    </div>
                    </hgroup>
                </div>
            </div>
        </div>
        <div class="item">

            <div class="slider_overlay"></div>
            <!-- Set the second background image using inline CSS below. -->
            <div class="fill" style="background-image:url('../img/slide2.jpg');">
                <div class="hero">
                    <hgroup>
                        <h1>RuleBlox</h1>
                        <h3> Sensors as a Service</h3>

                        <div class="text-vertical-center form-group col-sm-6 col-sm-offset-4">
                            <a href="#about" class="btn btn-dark btn-lg"><b>Learn More</b></a>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6 col-sm-offset-4">
                                {!! HTML::link(url('/auth/login'), Lang::get('Login')) !!}
                            </div>
                        </div>
                    </hgroup>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="slider_overlay"></div>
            <!-- Set the third background image using inline CSS below. -->
            <div class="fill" style="background-image:url('../img/slide3.jpg');">
                <div class="hero">
                    <hgroup>
                        <h1>RuleBlox</h1>
                        <h3> Sensors as a Service</h3>

                        <div class="text-vertical-center form-group col-sm-6 col-sm-offset-4">
                            <a href="#about" class="btn btn-dark btn-lg"><b>Learn More</b></a>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6 col-sm-offset-4">
                                {!! HTML::link(url('/auth/login'), Lang::get('Login')) !!}
                            </div>
                        </div>
                    </hgroup>
                </div>
            </div>
        </div>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#mainCarousel" data-slide="prev">
        <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#mainCarousel" data-slide="next">
        <span class="icon-next"></span>
    </a>

</header>







</body>

</html>
