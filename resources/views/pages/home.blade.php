<!DOCTYPE html>
<html>
@extends('header')

<!-- Mirrored from coderthemes.com/minton_1.7/dark_light/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 13 Oct 2016 11:12:01 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <link rel="shortcut icon" href="assets/images/favicon_1.ico">

    <title>Ruleblox</title>

    <link href="../plugins/switchery/switchery.min.css" rel="stylesheet" />
    <link href="../plugins/jquery-circliful/css/jquery.circliful.css" rel="stylesheet" type="text/css" />

    <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../css/core.css" rel="stylesheet" type="text/css">
    <link href="../css/icons.css" rel="stylesheet" type="text/css">
    <link href="../css/components.css" rel="stylesheet" type="text/css">
    <link href="../css/pages.css" rel="stylesheet" type="text/css">
    <link href="../css/menu.css" rel="stylesheet" type="text/css">
    <link href="../css/responsive.css" rel="stylesheet" type="text/css">

    <script src="../js/modernizr.min.js"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-72255993-1', 'auto');
        ga('send', 'pageview');
    </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->


</head>


<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left">
            <div class="text-center">
                <a href="index.html" class="logo"><i class="md md-equalizer"></i> <span>Ruleblox</span> </a>
            </div>
        </div>

        <!-- Navbar -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="">
                    <div class="pull-left">
                        <button class="button-menu-mobile open-left waves-effect">
                            <i class="md md-menu"></i>
                        </button>
                        <span class="clearfix"></span>
                    </div>

                    <form role="search" class="navbar-left app-search pull-left hidden-xs">
                        <input type="text" placeholder="Search..." class="form-control app-search-input">
                        <a href="#"><i class="fa fa-search"></i></a>
                    </form>

                    <ul class="nav navbar-nav navbar-right pull-right">

                        <li class="dropdown hidden-xs">
                            <div class="dropdown">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                            <!--<li>{!! HTML::link(url('/profile/'.Auth::user()->name), Lang::get('titles.profile')) !!}</li>-->
                                <li>{!! HTML::link(url('/auth/logout'), Lang::get('titles.logout')) !!}</li>
                            </ul>
                        </li>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0)"><i class="md md-face-unlock"></i> Profile</a></li>
                            <li><a href="javascript:void(0)"><i class="md md-settings"></i> Settings</a></li>
                            <li><a href="javascript:void(0)"><i class="md md-lock"></i> Lock screen</a></li>
                            <li><a href="javascript:void(0)"><i class="md md-settings-power"></i> Logout</a></li>
                        </ul>

                        </li>


                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
        </div>
    </div>
    <!-- Top Bar End -->


    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">

            <div id="sidebar-menu">
                <ul>
                    <li class="menu-title">Main</li>

                    <li>
                        <a href="" class="waves-effect waves-primary"><i class="md md-dashboard"></i><span> Dashboard </span></a>
                    </li>

                    <li class="">
                        <a href=""><i class="md md-palette"></i> <span> Data </span>
                        </a>
                    </li>

                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="user-detail">
            <div class="dropup">

                <ul class="dropdown-menu">
                    <li><a href="javascript:void(0)"><i class="md md-face-unlock"></i> Profile</a></li>
                    <li><a href="javascript:void(0)"><i class="md md-settings"></i> Settings</a></li>
                    <li><a href="javascript:void(0)"><i class="md md-lock"></i> Lock screen</a></li>
                    <li><a href="javascript:void(0)"><i class="md md-settings-power"></i> Logout</a></li>
                </ul>

            </div>
        </div>
    </div>



    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <ol class="breadcrumb pull-right">
                                <li><a href="#">RuleBlox</a></li>
                                <li class="active">Dashboard</li>
                            </ol>
                            <h4 class="page-title">Welcome !</h4>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12 text-center">
                        <section id="about" class="about">
                            <div class="container">
                                <div class="channel-details">
                                    <p>   Channel ID:
                                        <span id="channel-id" class="channel-data"></span>
                                    </p>
                                    <p> Channel Name:
                                        <span id="channel-name" class="channel-data"></span>
                                    </p>
                                    <p> Last Update:
                                        <span id="weather-data-time" class="channel-data"></span>
                                    </p>
                                </div>
                            </div>
                            <!-- /.row -->
                            <!-- /.container -->
                        </section>
                    </div>
                </div>

                <div class="row">

                    <section id="services" class="services">
                        <div class=" latest-readings container">
                            <div id="" class="row text-center content data-summary">
                                <div class="col-sm-6 col-lg-4">
                                    <div class="widget-simple-chart text-center card-box">
                                        <div><span class="fa-stack fa-4x">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-cloud fa-stack-1x text-primary"></i>
                                </span></div>
                                        <h3>Temperature</h3>
                                        <h3 id="temp" class="text-yellow"></h3>
                                        <p class="text-muted text-nowrap">DEGREES °C </p>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-lg-4">
                                    <div class="widget-simple-chart text-center card-box">
                              <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-compass fa-stack-1x text-primary"></i>
                            </span>
                                        <h3>Atmosperic Pressure</h3>
                                        <h3 id="pressure" class="text-primary "></h3>
                                        <p class="text-muted text-nowrap">kPa</p>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-lg-4">
                                    <div class="widget-simple-chart text-center card-box">
                            <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-tachometer fa-stack-1x text-primary"></i>
                            </span>
                                        <h3>Relative Humidity</h3>
                                        <h3 id="humidity" class="text-pink"></h3>
                                        <p class="text-muted text-nowrap">%</p>
                                    </div>
                                </div>

                            </div>
                            <!-- end row -->


                            <div class="row">

                                <div class="col-sm-6 col-lg-4">
                                    <div class="widget-simple-chart text-center card-box">
                                        <div>
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-tint fa-stack-1x text-primary"></i>
                                </span>
                                        </div>
                                        <h3>Dew Point</h3>
                                        <h3 id="dew" class="text-primary"></h3>
                                        <p class="text-muted text-nowrap">DEGREES °C </p>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-lg-4">
                                    <div class="widget-simple-chart text-center card-box">
                                        <div>
                                <span class="fa-stack fa-4x">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-bolt fa-stack-1x text-primary"></i>
                                </span>
                                            <h3>Supply Voltage</h3>
                                            <h3 id="voltage" class="text-primary"></h3>
                                            <p class="text-muted text-nowrap">V </p>
                                        </div>
                                    </div>


                                </div>
                                <!-- end row -->

                            </div>
                        </div>
                    </section>
                    <!-- end container -->
                </div>

                <!-- end content -->

                <footer class="footer text-right">
                    2016 © Ruleblox.
                </footer>

            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

        </div>
        <!-- end content -->

        <footer class="footer text-right">
            2016 © Minton.
        </footer>

    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


</div>
<!-- END wrapper -->



<script>
    var resizefunc = [];
</script>

<!-- Plugins  -->
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/detect.js"></script>
<script src="../js/fastclick.js"></script>
<script src="../js/jquery.slimscroll.js"></script>
<script src="../js/jquery.blockUI.js"></script>
<script src="../js/waves.js"></script>
<script src="../js/wow.min.js"></script>
<script src="../js/jquery.nicescroll.js"></script>
<script src="../js/jquery.scrollTo.min.js"></script>
<script src="../plugins/switchery/switchery.min.js"></script>

<!-- Counter Up  -->
<script src="../plugins/waypoints/lib/jquery.waypoints.js"></script>
<script src="../plugins/counterup/jquery.counterup.min.js"></script>

<!-- circliful Chart -->
<script src="../plugins/jquery-circliful/js/jquery.circliful.min.js"></script>
<script src="../plugins/jquery-sparkline/jquery.sparkline.min.js"></script>

<!-- skycons -->
<script src="../plugins/skyicons/skycons.min.js" type="text/javascript"></script>

<!-- Page js  -->
<script src="../pages/jquery.dashboard.js"></script>

<!-- Custom main Js -->
<script src="../js/jquery.core.js"></script>
<script src="../js/jquery.app.js"></script>


<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('.counter').counterUp({
            delay: 100,
            time: 1200
        });
        $('.circliful-chart').circliful();
    });

    // BEGIN SVG WEATHER ICON
    if (typeof Skycons !== 'undefined'){
        var icons = new Skycons(
                {"color": "#3bafda"},
                {"resizeClear": true}
                ),
                list  = [
                    "clear-day", "clear-night", "partly-cloudy-day",
                    "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                    "fog"
                ],
                i;

        for(i = list.length; i--; )
            icons.set(list[i], list[i]);
        icons.play();
    };

</script>

</body>

<!-- Mirrored from coderthemes.com/minton_1.7/dark_light/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 13 Oct 2016 11:12:57 GMT -->
</html>