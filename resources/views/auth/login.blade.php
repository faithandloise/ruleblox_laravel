@extends('app')

@section('template_title')
	Login
@endsection

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<div class="panel panel-default">
				<div class="panel-heading">{{ Lang::get('titles.login') }}</div>
				<div class="panel-body">

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<div class="form-group">
								<strong>{{ Lang::get('auth.whoops') }}</strong> {{ Lang::get('auth.someProblems') }}<br /><br />
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
									<li>{!! HTML::link(url('/password/email'), Lang::get('auth.forgot_message'), array('id' => 'forgot_message',)) !!}</li>
								</ul>

							</div>
						</div>
					@endif

					{!! Form::open(array('url' => 'auth/login', 'method' => 'POST', 'class' => 'lockscreen-credentials form-horizontal', 'role' => 'form')) !!}
						<div class="form-group has-feedback">
							{!! Form::label('email', Lang::get('auth.email') , array('class' => 'col-sm-4 control-label')); !!}
							<div class="col-sm-6">
								{!! Form::email('email', null, array('id' => 'email', 'class' => 'form-control', 'placeholder' => Lang::get('auth.ph_email'), 'required' => 'required',)) !!}
								<span class="glyphicon glyphicon-envelope form-control-feedback" aria-hidden="true"></span>
							</div>
						</div>
						<div class="form-group has-feedback">
							{!! Form::label('password', Lang::get('auth.password') , array('class' => 'col-sm-4 control-label')); !!}
							<div class="col-sm-6">
								{!! Form::password('password', array('id' => 'password', 'class' => 'form-control', 'placeholder' => Lang::get('auth.ph_password'), 'required' => 'required',)) !!}
								<span class="glyphicon glyphicon-lock form-control-feedback" aria-hidden="true"></span>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-sm-3  col-sm-offset-3">
								<div class="checkbox">
									{!! Form::checkbox('remember', 'remember', true, array('id' => 'remember')); !!}
									{!! Form::label('remember', Lang::get('auth.rememberMe')); !!}
								</div>
                                </div>
                               {{-- <div class="col-sm-3 col-sm-offset-1">
                                    {!! HTML::link(url('/password/email'), Lang::get('auth.forgot'), array('id' => 'forgot', 'class' => 'btn btn-link')) !!}

                                </div>--}}
							</div>

						<div class="form-group">
							<div class="col-sm-6 col-sm-offset-4">
								{!! Form::button(Lang::get('auth.login'), array('class' => 'btn btn-primary btn-block','type' => 'submit')) !!}


						</div>
						<div class="form-group">
							<div class="col-sm-6 col-sm-offset-5">
								{!! HTML::link(url('/auth/register'), Lang::get('titles.notreg')) !!}
							</div>
						</div>

					{!! Form::close() !!}

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
