<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes" name="viewport">
		<title>@if (trim($__env->yieldContent('template_title')))@yield('template_title') | @endif {{ Lang::get('titles.app') }}</title>

		{{-- FONTS --}}
		{!! HTML::style('//fonts.googleapis.com/css?family=Roboto:400,300', array('type' => 'text/css', 'rel' => 'stylesheet')) !!}
		@yield('template_linked_fonts')

		{{-- STYLESHEETS --}}
		{!! HTML::style(asset('/css/app.css'), array('type' => 'text/css', 'rel' => 'stylesheet')) !!}
		{!! HTML::style(asset('https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css'), array('type' => 'text/css', 'rel' => 'stylesheet')) !!}

		@yield('template_linked_css')

		<style type="text/css">
			@yield('template_fastload_css')
		</style>

	    {{-- HTML5 Shim and Respond.js for IE8 support --}}
	    <!--[if lt IE 9]>
	      {!! HTML::script('https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js', array('type' => 'text/javascript')) !!}
	      {!! HTML::script('//s3.amazonaws.com/nwapi/nwmatcher/nwmatcher-1.2.5-min.js', array('type' => 'text/javascript')) !!}
	      {!! HTML::script('//html5base.googlecode.com/svn-history/r38/trunk/js/selectivizr-1.0.3b.js', array('type' => 'text/javascript')) !!}
	      {!! HTML::script('https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js', array('type' => 'text/javascript')) !!}
	    <![endif]-->
	    <!--[if gte IE 9]>
	      <style type="text/css">.gradient {filter: none;}</style>
	    <![endif]-->



			<![endif]-->



	</head>
	<body>


		@yield('content')


		{{-- SCRIPTS --}}
		{!! HTML::script('//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js', array('type' => 'text/javascript')) !!}
		{!! HTML::script('//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js', array('type' => 'text/javascript')) !!}
		{!! HTML::script('//maps.googleapis.com/maps/api/js?key='.env("GOOGLEMAPS_API_KEY").'&libraries=places&dummy=.js', array('type' => 'text/javascript')) !!}
		@yield('template_scripts')
		<script src='https://www.google.com/recaptcha/api.js'></script>
		<script src='https://www.google.com/recaptcha/api.js?hl=es'></script>

		<script>
			var resizefunc = [];
		</script>

		<!-- Plugins  -->
		<script src="../js/jquery.min.js"></script>
		<script src="../js/bootstrap.min.js"></script>
		<script src="../js/detect.js"></script>
		<script src="../js/fastclick.js"></script>
		<script src="../js/jquery.slimscroll.js"></script>
		<script src="../js/jquery.blockUI.js"></script>
		<script src="../js/waves.js"></script>
		<script src="../js/wow.min.js"></script>
		<script src="../js/jquery.nicescroll.js"></script>
		<script src="../js/jquery.scrollTo.min.js"></script>
		<script src="../plugins/switchery/switchery.min.js"></script>

		<!-- Counter Up  -->
		<script src="../plugins/waypoints/lib/jquery.waypoints.js"></script>
		<script src="../plugins/counterup/jquery.counterup.min.js"></script>

		<!-- circliful Chart -->
		<script src="../plugins/jquery-circliful/js/jquery.circliful.min.js"></script>
		<script src="../plugins/jquery-sparkline/jquery.sparkline.min.js"></script>

		<!-- skycons -->
		<script src="../plugins/skyicons/skycons.min.js" type="text/javascript"></script>

		<!-- Page js  -->
		<script src="../pages/jquery.dashboard.js"></script>

		<!-- Custom main Js -->
		<script src="../js/jquery.core.js"></script>
		<script src="../js/jquery.app.js"></script>


		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$('.counter').counterUp({
					delay: 100,
					time: 1200
				});
				$('.circliful-chart').circliful();
			});

			// BEGIN SVG WEATHER ICON
			if (typeof Skycons !== 'undefined'){
				var icons = new Skycons(
						{"color": "#3bafda"},
						{"resizeClear": true}
						),
						list  = [
							"clear-day", "clear-night", "partly-cloudy-day",
							"partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
							"fog"
						],
						i;

				for(i = list.length; i--; )
					icons.set(list[i], list[i]);
				icons.play();
			};

		</script>

	</body>
</html>