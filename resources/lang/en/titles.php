<?php

return [

	'app'			=> 'Laravel',
	'app2'			=> 'Laravel 5',
	'home'			=> 'Home',
	'login'			=> 'LOGIN TO RULEBLOX',
	'logout'		=> 'Logout',
	'register'		=> 'SIGN UP FOR RULEBLOX ACCOUNT',
	'resetPword'	=> 'Reset Password',
	'toggleNav'		=> 'Toggle Navigation',
	'profile'		=> 'Profile',
	'editProfile'	=> 'Edit Profile',
	'createProfile'	=> 'Create Profile',
	'adminUserList'	=> 'Users List',
	'adminEditUsers'=> 'Edit Users',
	'adminNewUser'	=> 'Create New User',
	'notreg'		=>  'Not registered? click here to register',
	'notlogeed'     =>  'Already have an account? Log In'

];