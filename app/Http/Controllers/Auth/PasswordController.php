<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Traits\CaptchaTrait;

class PasswordController extends Controller {






	/*
	|--------------------------------------------------------------------------
	| Password Reset Controller
	|--------------------------------------------------------------------------
	|
	| This controller is responsible for handling password reset requests
	| and uses a simple trait to include this behavior. You're free to
	| explore this trait and override any methods you wish to tweak.
	|
	*/

	use ResetsPasswords;

	public function __construct()
	{
		$this->middleware($this->guestMiddleware());
	}

	protected function getSendResetLinkEmailSuccessResponse()
	{
		return redirect()->back()->with('info', 'A reset password link has been sent to the email address.');
	}

	protected function getResetSuccessResponse($response)
	{
		return redirect()->route('home')->with('info', 'Your password was changed.');
	}

}